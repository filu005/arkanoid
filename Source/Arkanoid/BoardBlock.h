// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "BoardBlock.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API ABoardBlock : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	ABoardBlock();

protected:
	virtual void BeginPlay() override;
	
public:
	UFUNCTION()
	const FBox& GetBoundingBox() const { return CollisionBoundingBox; }

private:
	FBox CollisionBoundingBox;
};
