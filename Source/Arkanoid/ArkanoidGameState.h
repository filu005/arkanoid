// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "ArkanoidGameState.generated.h"

class ABall;
class APaddle;
class ABricksManager;
class ACollisionDetectionSystem;
class ACollisionResponseSystem;

UCLASS()
class ARKANOID_API AArkanoidGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	AArkanoidGameState();

	void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	/** Spawns all Actors, sets up level, initiates collision system, registers events. */
	UFUNCTION()
	void InitGame();

	/** Desyroys Actors and re-Inits the game. */
	UFUNCTION()
	void Restart();

	/** Destroys all Actors created in InitGame() method. */
	UFUNCTION()
	void DestroyActors();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Blueprints")
	TSubclassOf<ABall> BallBP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Blueprints")
	TSubclassOf<APaddle> PaddleBP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Blueprints")
	TSubclassOf<ABricksManager> BricksManagerBP;

private:
	void RegisterEvents();

	/** Asks BricksManager to set up level. */
	UFUNCTION()
	void LoadLevel();

	UFUNCTION()
	void DrawCollisionGrid(const FVector2D& LowerLeftCorner, const FVector2D& UpperRightCorner) const;

	ABall* Ball;
	APaddle* Paddle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Actors", meta = (AllowPrivateAccess = "true"))
	ABricksManager* BricksManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collisions", meta = (AllowPrivateAccess = "true"))
	ACollisionDetectionSystem* CollisionDetectionSystem;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collisions", meta = (AllowPrivateAccess = "true"))
	ACollisionResponseSystem* CollisionResponseSystem;

	/** Switch for debug draw for collision grid. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collisions", meta = (AllowPrivateAccess = "true"))
	bool DrawCollisionGridDebug = false;
};
