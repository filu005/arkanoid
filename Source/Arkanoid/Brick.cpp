// Fill out your copyright notice in the Description page of Project Settings.

#include "Brick.h"
#include "Engine/StaticMesh.h"
#include "Components/InstancedStaticMeshComponent.h"

// Sets default values
ABrick::ABrick()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("BrickBase"));
	BrickMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BrickMesh"));
	BrickMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ABrick::BeginPlay()
{
	Super::BeginPlay();
	
	FVector PaddleOrigin, PaddleBoundsExtend;
	GetActorBounds(false, PaddleOrigin, PaddleBoundsExtend);
	CollisionBoundingBox = FBox::BuildAABB(PaddleOrigin, PaddleBoundsExtend);
}

// Called every frame
void ABrick::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
