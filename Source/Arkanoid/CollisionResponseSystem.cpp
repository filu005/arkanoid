// Fill out your copyright notice in the Description page of Project Settings.

#include "CollisionResponseSystem.h"
#include "DrawDebugHelpers.h"
#include "Ball.h"
#include "Brick.h"
#include "BoardBlock.h"
#include "Paddle.h"

ACollisionResponseSystem::ACollisionResponseSystem()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;
}

void ACollisionResponseSystem::SetPrequisiteCollisionDetectionActor(AActor* CollisionDetectionSystem)
{
	AddTickPrerequisiteActor(CollisionDetectionSystem);
}

void ACollisionResponseSystem::Tick(float DeltaTime)
{
	RespondToCollisions();
}

void ACollisionResponseSystem::Ball_Brick(ABall& Ball, ABrick& Brick, const FVector& SurfaceNormal)
{
	FVector NewVelocity = FMath::GetReflectionVector(Ball.Velocity, SurfaceNormal);
	Ball_BrickCollisionResponse.AddUnique(Ball_BrickResponse(&Ball, &Brick, NewVelocity));
	BrickDestroyEvent.Broadcast(&Brick);
}

void ACollisionResponseSystem::Ball_BoardBlock(ABall& Ball, ABoardBlock& BoardBlock, const FVector& SurfaceNormal)
{
	Ball.Velocity = FMath::GetReflectionVector(Ball.Velocity, SurfaceNormal);
}

void ACollisionResponseSystem::Ball_Paddle(ABall& Ball, APaddle& Paddle, const FVector& SurfaceNormal)
{
	FVector PaddleOrigin, PaddleBoundsExtend;
	Paddle.GetActorBounds(false, PaddleOrigin, PaddleBoundsExtend);

	// Offset from center of the Paddle
	float ContactPointOffset = Paddle.GetActorLocation().Y - Ball.GetActorLocation().Y;

	if(FMath::Abs(ContactPointOffset) > PaddleBoundsExtend.Y)
		return;

	//DrawDebugBox(world, PaddleOrigin, FVector(100.0f, 5.0f, 1.0f), FColor(1.0f), false, 5.0f);

	// interpolate between Min and Max bounce angle based on a Ball-Paddle contact point
	float BounceAngle = FMath::Lerp(Paddle.GetMinBounceAngle(), Paddle.GetMaxBounceAngle(), FMath::Abs(ContactPointOffset) / PaddleBoundsExtend.Y);
	BounceAngle *= FMath::Sign(ContactPointOffset);

	Ball.Velocity = SurfaceNormal.RotateAngleAxis(BounceAngle, FVector(1.0f, 0.0f, 0.0f)) * Ball.Velocity.Size();
}

void ACollisionResponseSystem::RespondToCollisions()
{
	// TODO: apply fix for 'internal edge problem' here
	if(Ball_BrickCollisionResponse.Num() > 1)
		UE_LOG(LogTemp, Warning, TEXT("RespondToCollisions; Collisions Num: %d"), Ball_BrickCollisionResponse.Num());

	for(auto Response : Ball_BrickCollisionResponse)
	{
		if(Response.Ball)
			Response.Ball->Velocity = Response.Velocity;
	}

	Ball_BrickCollisionResponse.Empty();
}