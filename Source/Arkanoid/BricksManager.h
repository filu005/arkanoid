// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Info.h"
#include "Platform.h"
#include "BricksManager.generated.h"

class UInstancedStaticMeshComponent;

/**
 * Class dedicated to managing life time of Bricks.
 * Responds to an event spawned on collision with a Ball.
 * It's tick fires late (in TG_PostUpdateWork tickGroup) to safely handle Brick destruction.
 */
UCLASS(Blueprintable)
class ARKANOID_API ABricksManager : public AInfo
{
	GENERATED_BODY()

public:
	ABricksManager();

	virtual void Tick(float DeltaTime) override;

	/**
	* Sets up a level: load bricks in proper places.
	* TODO: parse level from BP (via BrickManagerBP).
	*/
	UFUNCTION()
	void LayBricks();

	/** Destroys all existing Bricks and sets internal Brick counter to 0. */
	UFUNCTION()
	void DestroyAllBricks();

	/** This method */
	UFUNCTION()
	void OnBrickDestroy(ABrick* Brick);

	/** Returns Bricks count. Total number of Bricks is kept by internal counter. */
	UFUNCTION()
	int BrickCount() const { return NoBricks; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Blueprints")
	TSubclassOf<ABrick> BrickBP;

private:
	/** Destroys all Bricks which were picked for destruction in current frame. */
	void DestroyPresentFrameBricks();

	TArray<ABrick*> BricksToDestroy;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Brick Manager", meta = (AllowPrivateAccess = "true"))
	//UInstancedStaticMeshComponent* BrickInstancedMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Brick Manager", meta = (AllowPrivateAccess = "true"))
	int NoBricks = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Brick Manager", meta = (AllowPrivateAccess = "true"))
	int NoRows = 40;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Brick Manager", meta = (AllowPrivateAccess = "true"))
	int NoColumns = 28;
};
