// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Paddle.generated.h"

class UStaticMeshComponent;

USTRUCT(BlueprintType)
struct FPaddleInput
{
	GENERATED_BODY()
public:
	// 'Filtered' means clamped to [-1.0; 1.0] value
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Paddle Input")
	float FilteredMovementInputX;

	void MoveX(float AxisValue);
	void Filter();

private:
	float MovementInputX;
};

UCLASS()
class ARKANOID_API APaddle : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APaddle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	const FBox& GetBoundingBox() const { return CollisionBoundingBox; }

	UFUNCTION()
	float GetMinBounceAngle() const { return MinBounceAngle; }

	UFUNCTION()
	float GetMaxBounceAngle() const { return MaxBounceAngle; }

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Paddle Input")
	FPaddleInput PaddleInput;

	// Input Axis callback
	void PaddleMove(float AxisValue);

	// Recreate bounding box (f.ex after the Paddle has moved)
	void RegenerateBoundingBox();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Paddle", meta = (AllowPrivateAccess = "true"))
	FBox CollisionBoundingBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Paddle", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* PaddleMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Paddle", meta = (AllowPrivateAccess = "true"))
	float MinBounceAngle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Paddle", meta = (AllowPrivateAccess = "true"))
	float MaxBounceAngle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Paddle", meta = (AllowPrivateAccess = "true"))
	float MoveSpeed;
};
