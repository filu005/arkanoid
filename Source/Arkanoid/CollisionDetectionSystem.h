// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/Info.h"
#include "GameFramework/Actor.h"
#include "UniformGrid.h"
#include "Brick.h"
#include "CollisionDetectionSystem.generated.h"

class ABall;
class ABoardBlock;
class APaddle;
class ACollisionResponseSystem;

DECLARE_STATS_GROUP(TEXT("CollisionDetection"), STATGROUP_CollisionDetection, STATCAT_Advanced);

namespace CollisionDetectionUtils
{
	/**
	 * Modified FMath::SphereAABBIntersection() method; for description see "Math/Box.h"
	 * @param OutNormal[out] Will contain the normal of AABB where collision occured.
	 */
	bool SphereAABBIntersectionWithContactNormal(const FVector& SphereCenter, const float RadiusSquared, const FBox& AABB, FVector& OutNormal);
}

UCLASS()
class ARKANOID_API ACollisionDetectionSystem : public AInfo
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACollisionDetectionSystem();
	~ACollisionDetectionSystem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void InitUniformGrid(const FVector2D& LowerLeftCorner, const FVector2D& UpperRightCorner)
	{
		BrickUniformGrid.SetGridDimensions(LowerLeftCorner, UpperRightCorner);

		TArray<AActor*> Bricks;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABrick::StaticClass(), Bricks);

		BrickUniformGrid.InitGrid(Bricks);
	}

	UFUNCTION()
	void SetCollisionResponseSystemRef(ACollisionResponseSystem* CollisionResponseSystemRef)
	{
		CollisionResponseSystem = CollisionResponseSystemRef;
	}

	UFUNCTION()
	void OnBrickDestroy(ABrick* Brick);

private:
	/** Updates BrickUniformGrid; Removes from the grid all Actors (Bricks) which were flagged for deletion in present frame. */
	void RemoveActorsFromGrid();

	FUniformGrid2D BrickUniformGrid;
	TArray<ABrick*> BricksToDestroy;

	TArray<ABall*> Balls;
	TArray<APaddle*> Paddles;
	TArray<ABoardBlock*> BoardBlocks;

	ACollisionResponseSystem* CollisionResponseSystem = nullptr;
};
