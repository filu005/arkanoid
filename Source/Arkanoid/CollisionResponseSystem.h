// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Info.h"
#include "Ball.h"
#include "Brick.h"
#include "CollisionResponseSystem.generated.h"

class ABall;
class ABoardBlock;
class ABrick;
class APaddle;
class UWorld;

DECLARE_EVENT_OneParam(ACollisionResponseSystem, FBrickDestroyEvent, ABrick*);

struct Ball_BrickResponse
{
	Ball_BrickResponse(ABall* Ball, ABrick* Brick, FVector& Velocity) : Ball(Ball), Brick(Brick), Velocity(Velocity)
	{ }
	bool operator==(const Ball_BrickResponse& rhs) const
	{
		return *this->Ball == *rhs.Ball && *this->Brick == *rhs.Brick;
	}

	ABall* Ball;
	ABrick* Brick;
	FVector Velocity;
};
/**
 * 
 */
UCLASS()
class ARKANOID_API ACollisionResponseSystem : public AInfo
{
	GENERATED_BODY()
	
public:
	ACollisionResponseSystem();

	virtual void Tick(float DeltaTime) override;

	void SetPrequisiteCollisionDetectionActor(AActor* CollisionDetectionSystem);

	void Ball_Brick(ABall& Ball, ABrick& Brick, const FVector& SurfaceNormal);
	void Ball_BoardBlock(ABall& Ball, ABoardBlock& BoardBlock, const FVector& SurfaceNormal);
	void Ball_Paddle(ABall& Ball, APaddle& Paddle, const FVector& SurfaceNormal);

	void RespondToCollisions();

	FBrickDestroyEvent BrickDestroyEvent;

private:
	/** Array of collision responses to be applied at the end of Tick. */
	TArray<Ball_BrickResponse> Ball_BrickCollisionResponse;
};
