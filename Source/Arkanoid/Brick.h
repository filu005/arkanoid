// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Brick.generated.h"

class UInstancedStaticMeshComponent;

UCLASS()
class ARKANOID_API ABrick : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABrick();

	bool operator==(const ABrick& rhs) const
	{
		return GetUniqueID() == rhs.GetUniqueID();
	}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	const FBox& GetBoundingBox() const { return CollisionBoundingBox; }

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Brick", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BrickMesh;

	FBox CollisionBoundingBox;
};
