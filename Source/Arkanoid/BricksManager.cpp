// Fill out your copyright notice in the Description page of Project Settings.

#include "BricksManager.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Brick.h"

ABricksManager::ABricksManager()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostUpdateWork;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("BrickManagerBase"));
	//BrickInstancedMesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("BrickManagerInstancedMesh"));
	//BrickInstancedMesh->RegisterComponent();
	//BrickInstancedMesh->SetupAttachment(RootComponent);
	//BrickInstancedMesh->SetFlags(RF_Transactional);
	//BrickInstancedMesh->SetHiddenInGame(false);
	//BrickInstancedMesh->SetMobility(EComponentMobility::Static);
	//AddInstanceComponent(BrickInstancedMesh);
}

void ABricksManager::Tick(float DeltaTime)
{
	DestroyPresentFrameBricks();
}

void ABricksManager::LayBricks()
{
	if(BrickBP == nullptr)
		return;

	UWorld* world = GetWorld();

	// create a temporary Brick only to acquire its dimensions
	FVector BrickOrigin, BrickBoundsSize;
	ABrick* BrickTemp = world->SpawnActor<ABrick>(BrickBP, FVector(0.0f), FRotator::ZeroRotator);
	BrickTemp->GetActorBounds(false, BrickOrigin, BrickBoundsSize);
	BrickTemp->Destroy();
	BrickBoundsSize *= 2;

	FVector InitSpawnPosition(500.0f, -250.0f, -150.0f);

	for(int row = 0; row < NoRows; ++row)
	{
		FVector SpawnPosition(0.0f);

		SpawnPosition.X = InitSpawnPosition.X;

		SpawnPosition.Z = InitSpawnPosition.Z + BrickBoundsSize.Z * row;

		if(row % 2 == 0)
			InitSpawnPosition.Y += BrickBoundsSize.Y * 0.5f;
		else
			InitSpawnPosition.Y -= BrickBoundsSize.Y * 0.5f;

		for(int col = 0; col < NoColumns; ++col)
		{
			SpawnPosition.Y = InitSpawnPosition.Y + BrickBoundsSize.Y * col;

			ABrick* Brick = world->SpawnActor<ABrick>(BrickBP, SpawnPosition, FRotator::ZeroRotator);
			//BrickInstancedMesh->AddInstanceWorldSpace(FTransform(SpawnPosition + FVector(20.0f)));
			++NoBricks;
		}
	}
}

void ABricksManager::DestroyAllBricks()
{
	for(TActorIterator<ABrick> BrickIt(GetWorld()); BrickIt; ++BrickIt)
	{
		ABrick* Brick = *BrickIt;

		if(Brick)
		{
			Brick->Destroy();
			--NoBricks;
		}
	}
}

void ABricksManager::OnBrickDestroy(ABrick * Brick)
{
	BricksToDestroy.AddUnique(Brick);
}

void ABricksManager::DestroyPresentFrameBricks()
{
	for(auto Brick : BricksToDestroy)
	{
		if(Brick)
		{
			Brick->Destroy();
			--NoBricks;
		}
	}
	BricksToDestroy.Empty();
}