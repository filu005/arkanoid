// Fill out your copyright notice in the Description page of Project Settings.

#include "Ball.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "ArkanoidGameState.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ABall::ABall() : Velocity(0.0f), Radius(2.0f), InitialSpeed(200.0f), IsAttached(false)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SphereBase"));
	SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SphereMesh"));
	SphereMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
	// set Radius
	FVector BallOrigin, BallBoxExtend;
	UKismetSystemLibrary::GetComponentBounds(SphereMesh, BallOrigin, BallBoxExtend, Radius);
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// move
	FVector Pos = GetActorLocation();
	Pos += Velocity * DeltaTime;
	SetActorLocation(Pos);

	// check if ball didn't miss the paddle
	if(Pos.Z < -500.0f)
	{
		UWorld* world = GetWorld();

		AArkanoidGameState * GameState = world->GetGameState<AArkanoidGameState>();
		if(GameState)
			GameState->Restart();
	}
}

void ABall::Reset()
{
	UWorld* world = GetWorld();
	Velocity = FVector(0.0f);
	if(world)
	{
		APlayerController* Player = UGameplayStatics::GetPlayerController(this, 0);
		USceneComponent* StaticMeshSceneComponent = Cast<USceneComponent>(Player->GetPawn()->GetComponentByClass(UStaticMeshComponent::StaticClass()));
		if(StaticMeshSceneComponent)
		{
			AttachToComponent(StaticMeshSceneComponent, FAttachmentTransformRules::SnapToTargetIncludingScale, FName("BallSpawn"));
			IsAttached = true;
		}
	}
}

void ABall::DetachFromPaddle()
{
	if(IsAttached)
	{
		Velocity = InitialSpeed * FVector(0.0f, FMath::FRandRange(-0.9f, 0.9f), 1.0f);
		DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		IsAttached = false;
	}
}

