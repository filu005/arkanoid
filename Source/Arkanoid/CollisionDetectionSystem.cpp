// Fill out your copyright notice in the Description page of Project Settings.

#include "CollisionDetectionSystem.h"
#include "EngineUtils.h"
#include "UnrealMathUtility.h"
#include "CollisionResponseSystem.h"
#include "Arkanoid/Ball.h"
#include "Arkanoid/Paddle.h"
#include "Arkanoid/Brick.h"
#include "Arkanoid/BoardBlock.h"

DECLARE_CYCLE_STAT(TEXT("Collision Detection Tick"), STAT_CollisionDetectionTick, STATGROUP_CollisionDetection);
DECLARE_CYCLE_STAT(TEXT("Collision Detection Ball Brick"), STAT_CollisionDetectionBallBrick, STATGROUP_CollisionDetection);

namespace CollisionDetectionUtils
{
	bool SphereAABBIntersectionWithContactNormal(const FVector& SphereCenter, const float RadiusSquared, const FBox& AABB, FVector& OutNormal)
	{
		float DistSquared = 0.0f;

		if(SphereCenter.X < AABB.Min.X)
		{
			DistSquared += FMath::Square(SphereCenter.X - AABB.Min.X);
			OutNormal.X = -1.0f;
		}
		else if(SphereCenter.X > AABB.Max.X)
		{
			DistSquared += FMath::Square(SphereCenter.X - AABB.Max.X);
			OutNormal.X = 1.0f;
		}
		if(SphereCenter.Y < AABB.Min.Y)
		{
			DistSquared += FMath::Square(SphereCenter.Y - AABB.Min.Y);
			OutNormal.Y = -1.0f;
		}
		else if(SphereCenter.Y > AABB.Max.Y)
		{
			DistSquared += FMath::Square(SphereCenter.Y - AABB.Max.Y);
			OutNormal.Y = 1.0f;
		}
		if(SphereCenter.Z < AABB.Min.Z)
		{
			DistSquared += FMath::Square(SphereCenter.Z - AABB.Min.Z);
			OutNormal.Z = -1.0f;
		}
		else if(SphereCenter.Z > AABB.Max.Z)
		{
			DistSquared += FMath::Square(SphereCenter.Z - AABB.Max.Z);
			OutNormal.Z = 1.0f;
		}
		OutNormal = OutNormal.GetSafeNormal();
		// If the distance is less than or equal to the radius, they intersect
		return DistSquared <= RadiusSquared;
	}
}

ACollisionDetectionSystem::ACollisionDetectionSystem()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;
}

ACollisionDetectionSystem::~ACollisionDetectionSystem()
{
	BrickUniformGrid.ClearGrid();
	BoardBlocks.Empty();
	Balls.Empty();
	Paddles.Empty();
}

void ACollisionDetectionSystem::BeginPlay()
{
	Super::BeginPlay();

	UWorld* world = GetWorld();

	for(TActorIterator<ABoardBlock> BoardBlockIt(world); BoardBlockIt; ++BoardBlockIt)
		BoardBlocks.Add(*BoardBlockIt);

	for(TActorIterator<ABall> BallIt(GetWorld()); BallIt; ++BallIt)
		Balls.Add(*BallIt);

	for(TActorIterator<APaddle> PaddleIt(world); PaddleIt; ++PaddleIt)
		Paddles.Add(*PaddleIt);
}

// Called every frame
void ACollisionDetectionSystem::Tick(float DeltaTime)
{
	SCOPE_CYCLE_COUNTER(STAT_CollisionDetectionTick);

	Super::Tick(DeltaTime);

	// check Ball-Brick collision
	for(auto Ball : Balls)
	{
		SCOPE_CYCLE_COUNTER(STAT_CollisionDetectionBallBrick);
		auto NeighbourList = BrickUniformGrid.GetNeighboursList(Ball);

		for(auto NeighbouringCell : NeighbourList)
		{
			int BricksInCell = NeighbouringCell.NoElements;
			for(int BrickIdx = 0; BrickIdx < BricksInCell; ++BrickIdx)
			{
				ABrick* Brick = CastChecked<ABrick>(NeighbouringCell.Elements[BrickIdx]);
				FVector ContactNormal(0.0f);

				if(CollisionDetectionUtils::SphereAABBIntersectionWithContactNormal(Ball->GetActorLocation(), FMath::Square(Ball->Radius), Brick->GetBoundingBox(), ContactNormal))
				{
					CollisionResponseSystem->Ball_Brick(*Ball, *Brick, ContactNormal);
					break; // controversial but in Arkanoid it should work fine; or solve internal edge problem
				}
			}
		}
		//for(TActorIterator<ABrick> BrickIt(world); BrickIt; ++BrickIt)
		//{
		//	const ABrick* Brick = *BrickIt;
		//	FVector ContactNormal(0.0f);

		//	if(CollisionDetectionUtils::SphereAABBIntersectionWithContactNormal(Ball->GetActorLocation(), FMath::Square(Ball->Radius), Brick->GetBoundingBox(), ContactNormal))
		//		CollisionResponseSystem->Ball_Brick(**BallIt, **BrickIt, ContactNormal);
		//}
	}

	// check Ball-Paddle collision
	for(auto Paddle : Paddles)
	{
		for(auto Ball : Balls)
		{
			FSphere BallBoundingSphere(Ball->GetActorLocation(), Ball->Radius);

			if(FMath::SphereAABBIntersection(BallBoundingSphere, Paddle->GetBoundingBox()))
				CollisionResponseSystem->Ball_Paddle(*Ball, *Paddle, FVector(0.0f, 0.0f, 1.0f));
		}
	}

	// check Ball-Board collision
	for(auto Ball : Balls)
	{
		for(auto BoardBlock : BoardBlocks)
		{
			FVector ContactNormal(0.0f);

			//UE_LOG(LogTemp, Warning, TEXT("Ball_BoardBlock collision response! extent: %s"), *BoardBlock->GetBoundingBox().GetExtent().ToString());

			if(CollisionDetectionUtils::SphereAABBIntersectionWithContactNormal(Ball->GetActorLocation(), FMath::Square(Ball->Radius), BoardBlock->GetBoundingBox(), ContactNormal))
				CollisionResponseSystem->Ball_BoardBlock(*Ball, *BoardBlock, ContactNormal);
		}
	}

	RemoveActorsFromGrid();
}

void ACollisionDetectionSystem::OnBrickDestroy(ABrick* Brick)
{
	UE_LOG(LogTemp, Warning, TEXT("BricksManager::OnBrickDestroy"));
	BricksToDestroy.AddUnique(Brick);
}

void ACollisionDetectionSystem::RemoveActorsFromGrid()
{
	for(auto Brick : BricksToDestroy)
		BrickUniformGrid.RemoveElementFromGrid(Brick);

	BricksToDestroy.Empty();
}