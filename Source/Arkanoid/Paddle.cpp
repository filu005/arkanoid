// Fill out your copyright notice in the Description page of Project Settings.

#include "Paddle.h"
#include "Components/StaticMeshComponent.h"

void FPaddleInput::MoveX(float AxisValue)
{
	MovementInputX += AxisValue;
}

void FPaddleInput::Filter()
{
	FilteredMovementInputX = FMath::Clamp(MovementInputX, -1.0f, 1.0f);
	MovementInputX = 0.0f;
}

// Sets default values
APaddle::APaddle() : MinBounceAngle(0.0f), MaxBounceAngle(75.0f), MoveSpeed(250.0f)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("PaddleBase"));
	PaddleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PaddleMesh"));
	PaddleMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void APaddle::BeginPlay()
{
	Super::BeginPlay();

	RegenerateBoundingBox();
}

// Called every frame
void APaddle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	PaddleInput.Filter();

	// move paddle (only in horizontal [Y] axis)
	FVector Pos = GetActorLocation();
	Pos.Y += PaddleInput.FilteredMovementInputX * MoveSpeed * DeltaTime;
	Pos.Y = FMath::Clamp(Pos.Y, -270.0f, 270.0f); // FIXME: magic numbers; get real board dimensions
	SetActorLocation(Pos);

	RegenerateBoundingBox();
}

// Called to bind functionality to input
void APaddle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("PaddleMoveX", this, &APaddle::PaddleMove);
}

void APaddle::PaddleMove(float AxisValue)
{
	PaddleInput.MoveX(AxisValue);
}

void APaddle::RegenerateBoundingBox()
{
	FVector PaddleOrigin, PaddleBoundsExtend;
	GetActorBounds(false, PaddleOrigin, PaddleBoundsExtend);
	CollisionBoundingBox = FBox::BuildAABB(PaddleOrigin, PaddleBoundsExtend);
}
