// Fill out your copyright notice in the Description page of Project Settings.

#include "BoardBlock.h"

ABoardBlock::ABoardBlock()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ABoardBlock::BeginPlay()
{
	Super::BeginPlay();

	FVector BlockOrigin, BlockBoundsExtend;
	GetActorBounds(false, BlockOrigin, BlockBoundsExtend);
	CollisionBoundingBox = FBox::BuildAABB(BlockOrigin, BlockBoundsExtend * 1.1f);
}
