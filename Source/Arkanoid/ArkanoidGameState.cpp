// Fill out your copyright notice in the Description page of Project Settings.

#include "ArkanoidGameState.h"
#include <limits>
#include "EngineUtils.h"
#include "Engine.h"
#include "CollisionDetectionSystem.h"
#include "CollisionResponseSystem.h"
#include "BoardBlock.h"
#include "BricksManager.h"
#include "Paddle.h"
#include "Ball.h"

AArkanoidGameState::AArkanoidGameState()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AArkanoidGameState::BeginPlay()
{
	Super::BeginPlay();

	// setup game: create main objects (bricks, paddle and ball)
	InitGame();

	UE_LOG(LogTemp, Warning, TEXT("After Init"));
}

void AArkanoidGameState::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UWorld* world = GetWorld();

	// check if any of Bricks still exists
	if(BricksManager->BrickCount() <= 0)
		Restart();
}

void AArkanoidGameState::InitGame()
{
	UWorld* world = GetWorld();

	// get board dimensions from map created in the editor
	FVector LowerLeftBoardCorner(0.0f, std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	FVector UpperRightBoardCorner(0.0f, std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
	for(TActorIterator<ABoardBlock> BoardBlockIt(world); BoardBlockIt; ++BoardBlockIt)
	{
		const ABoardBlock* boardBlock = *BoardBlockIt;
		FVector BlockPosition, BlockBoundsExtend;
		boardBlock->GetActorBounds(false, BlockPosition, BlockBoundsExtend);
		UpperRightBoardCorner = UpperRightBoardCorner.ComponentMax(BlockPosition + BlockBoundsExtend);
		LowerLeftBoardCorner = LowerLeftBoardCorner.ComponentMin(BlockPosition - BlockBoundsExtend);
	}

	// create and place ball
	if(BallBP != nullptr)
		Ball = world->SpawnActor<ABall>(BallBP, FVector(0.0f), FRotator::ZeroRotator);

	// create and place bricks on the board
	if(BricksManagerBP != nullptr)
		BricksManager = world->SpawnActor<ABricksManager>(BricksManagerBP);

	LoadLevel();

	// create and place paddle
	if(PaddleBP != nullptr)
	{
		Paddle = world->SpawnActor<APaddle>(PaddleBP, FVector(500.0f, 0.0f, -300.0f), FRotator::ZeroRotator);
		UGameplayStatics::GetPlayerController(this, 0)->Possess(Paddle);

		// tie ball to paddle
		Ball->Reset();
	}

	// init collision systems with board dimensions
	CollisionDetectionSystem = world->SpawnActor<ACollisionDetectionSystem>();
	CollisionDetectionSystem->InitUniformGrid(FVector2D(LowerLeftBoardCorner.Y, LowerLeftBoardCorner.Z), FVector2D(UpperRightBoardCorner.Y, UpperRightBoardCorner.Z));
	CollisionResponseSystem = world->SpawnActor<ACollisionResponseSystem>();
	CollisionDetectionSystem->SetCollisionResponseSystemRef(CollisionResponseSystem);

	RegisterEvents();

	if(DrawCollisionGridDebug)
		DrawCollisionGrid(FVector2D(LowerLeftBoardCorner.Y, LowerLeftBoardCorner.Z), FVector2D(UpperRightBoardCorner.Y, UpperRightBoardCorner.Z));
}

void AArkanoidGameState::RegisterEvents()
{
	if(CollisionResponseSystem)
	{
		if(CollisionDetectionSystem)
			CollisionResponseSystem->BrickDestroyEvent.AddUObject(CollisionDetectionSystem, &ACollisionDetectionSystem::OnBrickDestroy);

		if(BricksManager)
			CollisionResponseSystem->BrickDestroyEvent.AddUObject(BricksManager, &ABricksManager::OnBrickDestroy);
	}
}

void AArkanoidGameState::Restart()
{
	DestroyActors();
	InitGame();
}

void AArkanoidGameState::DestroyActors()
{
	UWorld* world = GetWorld();

	if(Ball)
		Ball->Destroy();

	if(Paddle)
		Paddle->Destroy();

	if(CollisionDetectionSystem)
		CollisionDetectionSystem->Destroy();

	if(CollisionResponseSystem)
		CollisionResponseSystem->Destroy();

	if(BricksManager)
	{
		BricksManager->DestroyAllBricks();
		BricksManager->Destroy();
	}
}

void AArkanoidGameState::LoadLevel()
{
	if(BricksManager)
	{
		BricksManager->LayBricks();

		UE_LOG(LogTemp, Warning, TEXT("Bricks laid."));
	}
}

void AArkanoidGameState::DrawCollisionGrid(const FVector2D& LowerLeftCorner, const FVector2D& UpperRightCorner) const
{
	float dX = (UpperRightCorner.X - LowerLeftCorner.X) / static_cast<float>(c::K);
	float dY = (UpperRightCorner.Y - LowerLeftCorner.Y) / static_cast<float>(c::L);

	for(int x = LowerLeftCorner.X; x < UpperRightCorner.X; x += dX)
		DrawDebugLine(GetWorld(), FVector(500.0f, x, LowerLeftCorner.Y), FVector(500.0f, x, UpperRightCorner.Y), FColor::White, true);

	for(int y = LowerLeftCorner.Y; y < UpperRightCorner.Y; y += dY)
		DrawDebugLine(GetWorld(), FVector(500.0f, LowerLeftCorner.X, y), FVector(500.0f, UpperRightCorner.X, y), FColor::White, true);
}
