// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DrawDebugHelpers.h"
#include "Brick.h"

namespace c
{
	constexpr auto K = 32u, L = 32u;
	constexpr auto C = K * L;
}


struct GridCell
{
private:
	static constexpr short MaxNoElementsInGrid = 10;

public:
	GridCell() : NoElements(0)
	{
		Elements.SetNumZeroed(MaxNoElementsInGrid, false);
	}

	FORCEINLINE void AddElement(AActor* Actor)
	{
		checkf(NoElements < MaxNoElementsInGrid, TEXT("GridCell exceeded capacity; cannot add another Actor to array of size %i. Increase GridCell::MaxNoElementsInGrid."), Elements.Num());

		Elements[NoElements] = Actor;
		++NoElements;
	}

	/**
	 * Removing an element is simply overriting it by the last element in Elements array.
	 * Last element is pointed by NoElements so on remove it needs to be decremented by one.
	 */
	FORCEINLINE void RemoveElement(const AActor* Actor)
	{
		for(int ElementIdx = 0; ElementIdx < NoElements; ++ElementIdx)
		{
			if(Elements[ElementIdx] == Actor)
			{
				// like Elements.RemoveAtSwap(ElementIdx, 1, false);
				Elements[ElementIdx] = Elements[NoElements - 1];
				Elements[NoElements - 1] = nullptr;
				--NoElements;
				return;
			}
		}
	}

	// references to elements existing inside grid cell
	TArray<AActor*, TFixedAllocator<MaxNoElementsInGrid>> Elements;
	// elements count in the list
	size_t NoElements;
};
/**
 * Uniform Grid for collision detection. Grid's dimensions are set in compile time
 * (see c::K, c::L, GridCell::MaxNoElementsInGrid).
 * Whole grid resides in contigious memory (to boost iteration time).
 * TODO: sort GridCell's via Z-order curve for better cache coherence.
 */
class ARKANOID_API FUniformGrid2D
{
public:
	FUniformGrid2D()
		: XMin(0.0f), XMax(1.0f), YMin(0.0f), YMax(1.0f)
	{
		grid.SetNum(c::C, false);
		ClearGrid();
	}

	void SetGridDimensions(const FVector2D& LowerLeftCorner, const FVector2D& UpperRightCorner)
	{
		XMin = LowerLeftCorner.X; XMax = UpperRightCorner.X;
		YMin = LowerLeftCorner.Y; YMax = UpperRightCorner.Y;
		dX = (XMax - XMin) / static_cast<float>(c::K);
		dY = (YMax - YMin) / static_cast<float>(c::L);
		UE_LOG(LogTemp, Warning, TEXT("XMin, XMax: (%f, %f)"), XMin, XMax);
	}

	void InitGrid(const TArray<AActor*>& Elements)
	{
		for(auto & Element : Elements)
		{
			AABBSurface AABBSurf = GetAABBSurface(Element);

			// write pointer to Actor to all cells which are covered by its AABB surface
			for(float Col = AABBSurf.LowerLeftCorner.Y; Col <= AABBSurf.UpperRightCorner.Y; ++Col)
				for(float Row = AABBSurf.LowerLeftCorner.X; Row <= AABBSurf.UpperRightCorner.X; ++Row)
				{
					int c = GetCellIndexFromGridCoords(FVector2D(Row, Col));
					grid[c].AddElement(Element);
				}
		}
	}

	void RemoveElementFromGrid(const AActor* Element)
	{
		AABBSurface AABBSurf = GetAABBSurface(Element);

		// remove a reference (Actor*) from all cells which are covered by Actor's AABB surface
		for(float Col = AABBSurf.LowerLeftCorner.Y; Col <= AABBSurf.UpperRightCorner.Y; ++Col)
			for(float Row = AABBSurf.LowerLeftCorner.X; Row <= AABBSurf.UpperRightCorner.X; ++Row)
			{
				int c = GetCellIndexFromGridCoords(FVector2D(Row, Col));
				grid[c].RemoveElement(Element);
			}
	}

	FORCEINLINE TArray<GridCell> GetNeighboursList(const AActor* Element)
	{
		AABBSurface AABBSurf = GetAABBSurface(Element);

		// prepare an array of results
		TArray<GridCell> Neighbours;

		for(float Col = AABBSurf.LowerLeftCorner.Y; Col <= AABBSurf.UpperRightCorner.Y; ++Col)
			for(float Row = AABBSurf.LowerLeftCorner.X; Row <= AABBSurf.UpperRightCorner.X; ++Row)
			{
				int c = GetCellIndexFromGridCoords(FVector2D(Row, Col));

				// that only happens when Element is not in grid's area
				if(c >= c::C || c < 0)
					continue;

				if(grid[c].NoElements > 0)
					Neighbours.Add(grid[c]);
			}

		//UE_LOG(LogTemp, Warning, TEXT("No neighbours: %d"), Neighbours.Num());
		return Neighbours;
	}

	struct AABBSurface
	{
		FVector2D LowerLeftCorner;
		FVector2D UpperRightCorner;
	};

	/** Returns actor's AABB surface area. */
	FORCEINLINE AABBSurface GetAABBSurface(const AActor* Element)
	{
		FVector ActorPosition, ActorBoundsExtend;
		Element->GetActorBounds(false, ActorPosition, ActorBoundsExtend);
		FVector2D LowerLeftCorner, UpperRightCorner;
		LowerLeftCorner = GetGridCoords(FVector2D(ActorPosition.Y - ActorBoundsExtend.Y, ActorPosition.Z - ActorBoundsExtend.Z));
		UpperRightCorner = GetGridCoords(FVector2D(ActorPosition.Y + ActorBoundsExtend.Y, ActorPosition.Z + ActorBoundsExtend.Z));
		return { LowerLeftCorner, UpperRightCorner };
	}

	void ClearGrid()
	{
		for(auto & cell : grid)
		{
			for(auto & e : cell.Elements)
				e = nullptr;
			cell.NoElements = 0;
		}
	}

private:
	FORCEINLINE int GetCellIndex(const FVector2D& v) const
	{
		using namespace c;
		return static_cast<int>(
			FMath::FloorToFloat((v.X - XMin) / dX) +
			FMath::FloorToFloat((v.Y - YMin) / dY) * static_cast<float>(K)
			);
	}

	FORCEINLINE int GetCellIndexFromGridCoords(const FVector2D& v) const
	{
		return static_cast<int>(v.X + v.Y * static_cast<float>(c::K));
	}

	FORCEINLINE FVector2D GetGridCoords(const FVector2D& v) const
	{
		return FVector2D(FMath::FloorToFloat((v.X - XMin) / dX), FMath::FloorToFloat((v.Y - YMin) / dY));
	}

	TArray<GridCell, TFixedAllocator<c::C>> grid;
	float XMin, XMax, YMin, YMax;
	float dX = (XMax - XMin) / static_cast<float>(c::K);
	float dY = (YMax - YMin) / static_cast<float>(c::L);
};
